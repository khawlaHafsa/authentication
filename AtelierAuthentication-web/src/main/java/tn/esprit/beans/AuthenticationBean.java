package tn.esprit.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.persistence.Admin;
import tn.esprit.persistence.Admin.AdminNature;
import tn.esprit.persistence.Supplier;
import tn.esprit.persistence.User;

@SessionScoped
@ManagedBean(name = "AuthBean")
public class AuthenticationBean {
	
	private List<Admin> admins;
	private List<Supplier> suppliers;
	
	private Supplier supplierToAdd;
	
	private String login;
	private String password;
	
	private User currentUser;
	
	public AuthenticationBean() {
		Admin a = new Admin("Khawla", "Hafsa", new Date(1990, 5, 10), "admin@esprit.tn", "admin", AdminNature.superAdmin);
		
		admins  = new ArrayList<Admin>();
		suppliers = new ArrayList<Supplier>();
		
		admins.add(a);
		
		supplierToAdd = new Supplier();
	}
	
	public String doLogin()
	{
		int i = 0;
		
		while(i < admins.size() && currentUser == null)
		{
			if(admins.get(i).getMail().equals(login) && admins.get(i).getPassword().equals(password))
				{
					currentUser = admins.get(i);
				}
			
			i++;
		}
		
		if(currentUser != null)
		{
			return "AdminHome?faces-redirect=true";
		}
		
		i = 0;
		
		
		
		while(i < suppliers.size() && currentUser == null)
		{	
			if(suppliers.get(i).getMail().equals(login) && suppliers.get(i).getPassword().equals(password))
				currentUser = suppliers.get(i);
			
			i++;
		}
		
		if(currentUser != null)
		{
			return "SupplierHome?faces-redirect=true";
		}
		else
		{
			return "Login?faces-redirect=true";
		}
		
	}
	
	public String doRegister()
	{
		
		suppliers.add(supplierToAdd);
		
		return "Login?faces-redirect=true";
		
	}
	
	public String doLogout() {
		FacesContext.getCurrentInstance().getExternalContext()
				.invalidateSession();
		return "Login.jsf";
	}
	

	public List<Admin> getAdmins() {
		return admins;
	}

	public void setAdmins(List<Admin> admins) {
		this.admins = admins;
	}

	public List<Supplier> getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(List<Supplier> suppliers) {
		this.suppliers = suppliers;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public Supplier getSupplierToAdd() {
		return supplierToAdd;
	}

	public void setSupplierToAdd(Supplier supplierToAdd) {
		this.supplierToAdd = supplierToAdd;
	}
	
	
	
	
	

}
