package tn.esprit.persistence;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3464784566794073032L;
	
	private String firstName;
	private String lastName;
	private Date birthDate;
	private String mail;
	private String password;
	
	
	
	public User() {
		super();
	}


	public User(String firstName, String lastName, Date birthDate, String mail,
			String password) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.mail = mail;
		this.password = password;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Date getBirthDate() {
		return birthDate;
	}


	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName
				+ ", birthDate=" + birthDate + ", mail=" + mail + ", password="
				+ password + "]";
	}
	
	

}
