package tn.esprit.persistence;

import java.util.Date;

public class Supplier extends User{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8222052097335387264L;
	private String factory;
	private String factoryAdress;
	
	

	public Supplier() {
		super();
	}

	public Supplier(String firstName, String lastName, Date birthDate,
			String mail, String password, String factory, String factoryAdress) {
		super(firstName, lastName, birthDate, mail, password);
		
		this.factory = factory;
		this.factoryAdress = factoryAdress;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public String getFactoryAdress() {
		return factoryAdress;
	}

	public void setFactoryAdress(String factoryAdress) {
		this.factoryAdress = factoryAdress;
	}

	@Override
	public String toString() {
		return super.toString() + " Supplier [factory=" + factory + ", factoryAdress="
				+ factoryAdress + "]";
	}
	
	
	

}
