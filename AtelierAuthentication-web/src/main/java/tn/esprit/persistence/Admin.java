package tn.esprit.persistence;

import java.util.Date;

public class Admin extends User{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8229342868162052408L;

	public enum AdminNature {superAdmin, admin}
	
	private AdminNature nature;

	public Admin(String firstName, String lastName, Date birthDate,
			String mail, String password, AdminNature nature) {
		super(firstName, lastName, birthDate, mail, password);
		
		this.nature = nature;
	}

	public AdminNature getNature() {
		return nature;
	}

	public void setNature(AdminNature nature) {
		this.nature = nature;
	};
	
	
	
	
	

}
